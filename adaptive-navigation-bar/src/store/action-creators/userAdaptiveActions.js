import { DATA_ANALYST_TYPES } from "../action-types";
import { SALES_MANAGER_TYPES } from "../action-types";

export const compareWeights = ( a, b) => {
    return b.weight-a.weight;
}

export const sortLinkStore = (user, linkStore) => {
    return {
        type: user ? SALES_MANAGER_TYPES.SORT_LINK_STORE_S : DATA_ANALYST_TYPES.SORT_LINK_STORE_D,
        payload: linkStore.sort(compareWeights)
    }
}

export const addWeight = (user, key) => {
    return {
        type: user ? SALES_MANAGER_TYPES.ADD_WEIGHT_S : DATA_ANALYST_TYPES.ADD_WEIGHT_D,
        payload: key
    }
}

export const setImportantLink = (user, link) => {
    return {
        type: user ? SALES_MANAGER_TYPES.SET_IMPORTANT_LINK_S : DATA_ANALYST_TYPES.SET_IMPORTANT_LINK_D,
        payload: link
    }
}