import { GENERAL_TYPES } from "../action-types";

export const toggleUser = () => {
    return {
        type: GENERAL_TYPES.TOGGLE_USER
    }
}