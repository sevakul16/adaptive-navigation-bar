import { GENERAL_TYPES } from "../action-types"; 

const initialState = {
    selectedUser: false
}

const generalReducer = (state = initialState, action) => {
    switch (action.type) {
        case GENERAL_TYPES.TOGGLE_USER:
            return {
                ...state,
                selectedUser: !state.selectedUser
            }
        default:
            return state;
    }
}

export default generalReducer;