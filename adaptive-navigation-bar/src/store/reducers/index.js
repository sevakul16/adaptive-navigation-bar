import { combineReducers } from "redux";
import generalReducer from "./generalReducers";
import dataAnalystReducer from "./dataAnalystReducer";
import salesManagerReducer from "./salesManagerReducer";

const allReducers = combineReducers({
    general: generalReducer,
    dataAnalyst: dataAnalystReducer,
    salesManager: salesManagerReducer
})

export default allReducers;