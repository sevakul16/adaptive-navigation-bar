import { DATA_ANALYST_TYPES } from "../action-types"; 
import { compareWeights } from "../action-creators/userAdaptiveActions";

const initialState = {
    personalContent: 'Content for Data Analyst',
    linkStore: [
        {
            key: 0,
            name: 'Worklist',
            link: '/worklist',
            weight: 3
        },
        {
            key: 1,
            name: 'Analytics',
            link: '/analytics',
            weight: 5
        },
        {
            key: 2,
            name: 'Incoming orders',
            link: '/incoming_orders',
            weight: 1
        },
        {
            key: 3,
            name: 'Customer reviews',
            link: '/customer_reviews',
            weight: 1
        },
        {
            key: 4,
            name: 'Activities',
            link: '/activities',
            weight: 6
        }
    ],
    importantLink: null
}

const dataAnalystReducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_ANALYST_TYPES.SORT_LINK_STORE_D:
            return{
                ...state,
                linkStore: action.payload
            }
        case DATA_ANALYST_TYPES.ADD_WEIGHT_D:
            const index = state.linkStore.findIndex(element => element.key === action.payload)
            return{
                ...state,
                linkStore: state.linkStore[index].weight++
            }
        case DATA_ANALYST_TYPES.SET_IMPORTANT_LINK_D:
            return{
                ...state,
                importantLink:action.payload
            }
        default:
            return{
                ...state,
                linkStore: state.linkStore.sort(compareWeights)
            }
    }
}

export default dataAnalystReducer;