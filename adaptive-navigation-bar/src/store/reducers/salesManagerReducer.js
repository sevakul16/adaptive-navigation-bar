import { SALES_MANAGER_TYPES } from "../action-types";
import { compareWeights } from "../action-creators/userAdaptiveActions";

const initialState = {
    personalContent: 'Content for Sales Manager',
    linkStore: [
        {
            key: 0,
            name: 'Activities',
            link: '/activities',
            weight: 6
        },
        {
            key: 1,
            name: 'Customer reviews',
            link: '/customer_reviews',
            weight: 1
        },
        {
            key: 2,
            name: 'Incoming orders',
            link: '/incoming_orders',
            weight: 1
        },
        {
            key: 3,
            name: 'Sales managment',
            link: '/sales_managment',
            weight: 5
        },
        {
            key: 4,
            name: 'Worklist',
            link: '/worklist',
            weight: 3
        }
    ],
    importantLink: null
}

const salesManagerReducer = (state = initialState, action) => {
    switch (action.type) {
        case SALES_MANAGER_TYPES.SORT_LINK_STORE_S:
            return{
                ...state,
                linkStore: action.payload
            }
        case SALES_MANAGER_TYPES.ADD_WEIGHT_S:
            const index = state.linkStore.findIndex(element => element.key === action.payload)
            return{
                ...state,
                linkStore: state.linkStore[index].weight++
            }
        case SALES_MANAGER_TYPES.SET_IMPORTANT_LINK_S:
            return{
                ...state,
                importantLink:action.payload
            }
        default:
            return{
                ...state,
                linkStore: state.linkStore.sort(compareWeights)
            }
    }
}

export default salesManagerReducer;