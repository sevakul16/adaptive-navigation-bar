import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const SalesManagement = () => {
    return(
        <Box height="100vh" sx={{ background: '#e7e393'}}>
            <Typography textAlign='center' variant="h1" sx={{pt: 25}}>
                Sales Management
            </Typography>
        </Box>
    )
}

export default SalesManagement;