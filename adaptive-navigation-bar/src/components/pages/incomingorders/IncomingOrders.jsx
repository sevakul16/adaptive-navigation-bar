import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const IncomingOrders = () => {
    return(
        <Box height="100vh" sx={{ background: '#cbb3bf'}}>
            <Typography textAlign='center' variant="h1" sx={{pt: 25}}>
                Incoming Orders
            </Typography>
        </Box>
    )
}

export default IncomingOrders;