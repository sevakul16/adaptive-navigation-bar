import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const CustomerReviews = () => {
    return(
        <Box height="100vh" sx={{ background: '#dbc7be'}}>
            <Typography textAlign='center' variant="h1" sx={{pt: 25}}>
                Customer Reviews            
            </Typography>
        </Box>
    )
}

export default CustomerReviews;