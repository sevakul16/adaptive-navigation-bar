import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const Analytics = () => {
    return(
        <Box height="100vh" sx={{ background: '#95adb6'}}>
            <Typography textAlign='center' variant="h1" sx={{pt: 25}}>
                Analytics
            </Typography>
        </Box>
    )
}

export default Analytics;