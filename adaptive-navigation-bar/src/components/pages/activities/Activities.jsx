import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const Activities = () => {
    return(
        <Box height="100vh" sx={{ background: '#ef959c'}}>
            <Typography textAlign='center' variant="h1" sx={{pt: 25}}>
                Activities            
            </Typography>
        </Box>
    )
}

export default Activities;