import { Box } from "@mui/system";
import { Typography } from "@mui/material";
import { useSelector } from "react-redux";

const HomePage = () => {
    const { selectedUser } = useSelector(state => state.general);
    const { personalContent } = useSelector(state => selectedUser ? state.salesManager : state.dataAnalyst);
    return(
        <Box height="100vh" sx={{ background: '#FFFFFF'}} textAlign='center'>
            <Typography variant="h1" sx={{pt: 25}}>
                Home Page
            </Typography>
            <Typography variant="h4">
                { personalContent }
            </Typography>
        </Box>
    )
}

export default HomePage;