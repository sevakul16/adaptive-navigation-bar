import { Box } from "@mui/system";
import { Typography } from "@mui/material";

const Worklist = () => {
    return(
        <Box height="100vh" sx={{ background: '#8da1b9'}}>
            <Typography textAlign='center' variant="h1" sx={{pt: 25}}>
                Worklist
            </Typography>
        </Box>
    )
}

export default Worklist;