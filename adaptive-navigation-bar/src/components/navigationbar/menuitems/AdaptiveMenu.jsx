import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import '../styles.css';
import { useState, useEffect } from "react";
import { addWeight, sortLinkStore, setImportantLink } from "../../../store/action-creators/userAdaptiveActions";

const AdaptiveMenu = () => {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const { selectedUser } = useSelector(state => state.general)
    const { linkStore, importantLink } = useSelector(state => selectedUser ? state.salesManager : state.dataAnalyst);
    const dispatch = useDispatch();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
      };

    const handleClose = () => {
        setAnchorEl(null);
      };

    const handleLinkClick = (key) => {
        dispatch( addWeight(selectedUser, key));
        dispatch( sortLinkStore(selectedUser, linkStore));
    }

    useEffect(()=>{
        if (importantLink === null && linkStore[0].weight >= 10) {
            dispatch(setImportantLink(selectedUser, linkStore[0]))
        } 
    })

    return(
        <Box>
            <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 2 }}
                onClick={handleClick}
            >
                <MenuIcon />
            </IconButton>
            <Menu  
                sx={{ mt: '45px' }}
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
            >
            <Link style={{ textDecoration: 'none' }} onClick={() => handleLinkClick(linkStore[0].key)} to={linkStore[0].link}>
                <MenuItem style={{color: '#1976D2', fontWeight: 'bold'}}>
                    {linkStore[0].name}
                </MenuItem>
            </Link>
            {
                linkStore.slice(1).map(({key, name, link}) => {
                    return(
                        <Link key={key} onClick={() => handleLinkClick(key)} className='Link' to={link}>
                            <MenuItem>
                                {name}
                            </MenuItem>
                        </Link>
                    )
                })
            }
            </Menu>
        </Box>
    )
}

export default AdaptiveMenu;