import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import { Link, useNavigate } from 'react-router-dom';
import './styles.css';
import { useSelector, useDispatch } from 'react-redux';
import { toggleUser } from '../../store/action-creators/generalActions';
import AdaptiveMenu from './menuitems/AdaptiveMenu';

const NavigationBar = () => {
    const navigate = useNavigate();
    const { selectedUser } = useSelector(state => state.general);
    const { importantLink } = useSelector(state => selectedUser ? state.salesManager : state.dataAnalyst);
    const dispatch = useDispatch();

    const handleUserChange = () => {
        dispatch(toggleUser());
        navigate('/');
    };

    return(
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Link className='Link' to='/'>
                        <Typography textAlign='left' variant="h5" component="div" >
                            Home
                        </Typography>
                    </Link>
                    {
                        importantLink !== null ? 
                            <Link className='Link' to={importantLink.link}>
                                <Typography style={{ color: '#FCBA49' }} pl={4} variant="h5" component="div">
                                    {importantLink.name}
                                </Typography>
                            </Link>
                        :
                            null
                    }
                    <Box 
                        sx={{ flexGrow: 1 }} 
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <FormGroup>
                            <FormControlLabel
                            control={
                                <Switch
                                checked={selectedUser}
                                aria-label="login switch"
                                onChange={handleUserChange}
                                />
                            }
                            label={selectedUser ? 'Sales Manager' : 'Data Analyst'}
                            />
                        </FormGroup>
                    </Box>
                    <AdaptiveMenu/>
                </Toolbar>
            </AppBar>
        </Box>
    )
}

export default NavigationBar;