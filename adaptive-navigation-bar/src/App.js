import './App.css';
import NavigationBar from './components/navigationbar/NavigationBar';
import { Routes, Route} from 'react-router-dom';
import HomePage from './components/pages/homepage/HomePage';
import Worklist from './components/pages/worklist/Worklist';
import Analytics from './components/pages/analytics/Analytics';
import IncomingOrders from './components/pages/incomingorders/IncomingOrders';
import CustomerReviews from './components/pages/customerreviews/CustomerReviews';
import Activities from './components/pages/activities/Activities';
import SalesManagement from './components/pages/salesmanagement/SalesManagemnet';

function App() {
  return (
    <div>
      <NavigationBar/>
      <Routes>
        <Route path='/' element={<HomePage/>}/>
        <Route path='/worklist' element={<Worklist/>}/>
        <Route path='/analytics' element={<Analytics/>}/>
        <Route path='/incoming_orders' element={<IncomingOrders/>}/>
        <Route path='/customer_reviews' element={<CustomerReviews/>}/>
        <Route path='/activities' element={<Activities/>}/>
        <Route path='/sales_managment' element={<SalesManagement/>}/>
      </Routes>
    </div>
  );
}

export default App;
